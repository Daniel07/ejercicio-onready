package main;

import herramientasAuxiliares.ConstructorDeVehiculos;
import model.Concesionaria;

public class Main {

	public static void main(String[] args) {
		
		Concesionaria c = new Concesionaria();
		c.agregarVehiculos(ConstructorDeVehiculos.obtenerVehiculos());
		
		c.mostrarVehiculos();
		System.out.println("=============================");
		c.mostrarVehiculoMasCaro();
		c.mostrarVehiculoMasBarato();
		c.mostrarVehiculoSegunLetraEnElModelo("Y");
		System.out.println("=============================");
		c.mostrarVehiculosOrdenadosPorPrecioMayorMenor();
		
	}

}
