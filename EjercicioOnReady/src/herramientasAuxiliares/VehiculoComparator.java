package herramientasAuxiliares;

import java.util.Comparator;

import model.Vehiculo;

public class VehiculoComparator {

	private VehiculoComparator() {
	}
	
	public static Comparator<Vehiculo> precioMenorMayor()
	{
		return (uno, otro) -> (int) (uno.getPrecio() - otro.getPrecio());
	}

	public static Comparator<Vehiculo> precioMayorMenor()
	{
		return (uno, otro) -> (int) (otro.getPrecio() - uno.getPrecio());
	}
}
