package herramientasAuxiliares;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Matcheador {

	private Matcheador(){
		
	}
	
	public static boolean estaContenidaEn(String s1, String s2) {
		Matcher m = obtenerMatcher(s1, s2);
		return m.find();
	}

	private static Matcher obtenerMatcher(String s1, String s2) {
		Pattern pattern = Pattern.compile(s1);
		return pattern.matcher(s2);
	}
}
