package herramientasAuxiliares;

import java.util.ArrayList;

import model.Auto;
import model.Moto;
import model.Vehiculo;

public class ConstructorDeVehiculos {

	private static ArrayList<Vehiculo> vehiculos = new ArrayList<Vehiculo>();
	
	private ConstructorDeVehiculos() {
		
	}
	
	public static ArrayList<Vehiculo> obtenerVehiculos(){
		
		if(vehiculos.isEmpty()) {
			cargarVehiculos();
		}
		return vehiculos;
	}

	private static void cargarVehiculos() {
		
		Auto a1 = new Auto("Peugeot", "206", 4, 200000);
		Moto m1 = new Moto("Honda", "Titan", 125, 60000);
		Auto a2 = new Auto("Peugeot", "208", 5, 250000);
		Moto m2 = new Moto("Yamaha", "YBR", 160, 80500.50);

		vehiculos.add(a1);
		vehiculos.add(m1);
		vehiculos.add(a2);
		vehiculos.add(m2);
	}
}
