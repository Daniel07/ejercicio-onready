package herramientasAuxiliares;

import java.text.DecimalFormat;

public class Formateador {

	private Formateador() {
		
	}
	
	static public String formatearPrecio(double precio) {
		
		DecimalFormat format = new DecimalFormat("###,###.00");
		return format.format(precio);
		
	}
}
