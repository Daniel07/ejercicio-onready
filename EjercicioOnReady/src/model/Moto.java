package model;

import herramientasAuxiliares.Formateador;

public class Moto extends Vehiculo{

	private int cilindrada;
	
	public Moto(String marca, String modelo, int cilindrada, double precio) {
		super(marca, modelo, precio);
		this.cilindrada = cilindrada;
	}

	public int getCilindrada() {
		return cilindrada;
	}

	public void setCilindrada(int cilindrada) {
		this.cilindrada = cilindrada;
	}
	
	@Override
	public String toString() {
		
		return String.format("Marca: %s // Modelo: %s // Cilindrada: %dc // Precio: $%s", 
				marca, modelo, cilindrada, Formateador.formatearPrecio(precio));
	}
}
