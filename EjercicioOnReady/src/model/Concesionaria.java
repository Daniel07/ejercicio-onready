package model;

import java.util.ArrayList;
import herramientasAuxiliares.Formateador;
import herramientasAuxiliares.Matcheador;
import herramientasAuxiliares.VehiculoComparator;

public class Concesionaria {

	private ArrayList<Vehiculo> vehiculos;
	
	public Concesionaria() {
		
		vehiculos = new ArrayList<Vehiculo>();
	}
	
	public void agregarVehiculo(Vehiculo v){
		
		if(!vehiculos.contains(v) && v != null)
			vehiculos.add(v);
	
	}
	
	public void agregarVehiculos(ArrayList<Vehiculo> vehiculos){
		
		if(vehiculos != null)
			this.vehiculos = vehiculos;
	
	}
	
	public void mostrarVehiculos() {
		
		for (Vehiculo vehiculo : vehiculos) {
			System.out.println(vehiculo);
		}
	}
	
	public void mostrarVehiculosOrdenadosPorPrecioMayorMenor() {
		
		ArrayList<Vehiculo> vehiculosAux = ordenarVehiculosPorPrecio();
		System.out.println("Veh�culos ordenados por precio de mayor a menor:");
		
		for (Vehiculo v : vehiculosAux) {
			System.out.println(v.getMarca() + " " + v.getModelo());
		}
	}
	
	public void mostrarVehiculoMasCaro() {
		
		Vehiculo v = obtenerVehiculoMasCaro();
		System.out.println("Veh�culo m�s caro: " + v.getMarca() + " " + v.getModelo());
	}
	
	public void mostrarVehiculoMasBarato() {
		
		Vehiculo v = obtenerVehiculoMasBarato();
		System.out.println("Veh�culo m�s barato: " + v.getMarca() + " " + v.getModelo());
	}
	
	private Vehiculo obtenerVehiculoMasCaro() {
		
		return ordenarVehiculosPorPrecio().get(0);
	}
	
	private Vehiculo obtenerVehiculoMasBarato() {
		
		return ordenarVehiculosPorPrecio().get(vehiculos.size()-1);
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Vehiculo> ordenarVehiculosPorPrecio() {
		
		ArrayList<Vehiculo> vehiculosAux = (ArrayList<Vehiculo>) vehiculos.clone();
		vehiculosAux.sort(VehiculoComparator.precioMayorMenor());
		return vehiculosAux;
		
	}
	
	public void mostrarVehiculoSegunLetraEnElModelo(String c) {
		
		Vehiculo v = obtenerVehiculoSegunLetraEnElModelo(c);
		if(v != null)
			System.out.println(String.format("Veh�culo que contiene en su modelo a la letra '%s' : %s %s $%s", 
					c, v.getMarca(), v.getModelo(), Formateador.formatearPrecio(v.getPrecio())));
	}

	private Vehiculo obtenerVehiculoSegunLetraEnElModelo(String c) {
		
		for (Vehiculo v : vehiculos) {
			if(Matcheador.estaContenidaEn(c, v.getModelo()))
				return v;
		}
		 
		return null;
	}
	
	
}
