package model;

import herramientasAuxiliares.Formateador;

public class Auto extends Vehiculo{

	private int puertas;
	
	public Auto(String marca, String modelo, int puertas, double precio) {
		super(marca, modelo, precio);
		this.puertas = puertas;
	}

	public int getPuertas() {
		return puertas;
	}

	public void setPuertas(int puertas) {
		this.puertas = puertas;
	}

	@Override
	public String toString() {
		
		return String.format("Marca: %s // Modelo: %s // Puertas: %d // Precio: $%s", 
				marca, modelo, puertas, Formateador.formatearPrecio(precio));
	}
	
	
}
